package com.example.qhq;

public class List_Item {

    private String Head ;
    private String Des ;

    public List_Item(String head, String des) {
        Head = head;
        Des = des;
    }

    public String getHead() {
        return Head;
    }

    public String getDes() {
        return Des;
    }
}
