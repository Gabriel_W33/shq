package com.example.qhq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView ;
    private RecyclerView.Adapter adapter ;

    private List<List_Item> listItems ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        recyclerView .setHasFixedSize(true);
        recyclerView .setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();

        for(int i=0; i<=100 ; i++ ){
            List_Item listItem = new List_Item(
                    "bla" + i,
                    "coś raz" +(i*i)
            );
            listItems.add(listItem);
        }

        adapter = new Adapter(listItems , this);
        recyclerView.setAdapter(adapter);


    }
}
